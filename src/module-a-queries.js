const {createClient} = require('./redis-client');
const {promisify} = require('util');
const logger = require('./logger');

const HOST = 'redis://:0GpiRJI6MOeomPvzJ5AnMVd858q0ciIs@redis-17799.c55.eu-central-1-1.ec2.cloud.redislabs.com:17799';
const TTL = 1000;

const redisClient = createClient(HOST);


const formatRedisKey = (sid) => {
  return `a:${sid}`;
};

const setSessionInfo = async (sessionId, sessionDetails) => {
  try {
    const setAsync = promisify(redisClient.psetex).bind(redisClient);

    console.info('Writing session info');
    await setAsync(formatRedisKey(sessionId), TTL, JSON.stringify(sessionDetails));

  } catch (err) {
    logger.error('Redis error', {
      error: err ? JSON.stringify(err) : null,
      stack: err ? err.stack : null,
      host:HOST,
    });
    throw err;
  }
};

module.exports = {
  setSessionInfo
};
