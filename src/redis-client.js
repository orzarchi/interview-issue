const redis = require('redis');
const logger = require('./logger');

function createClient(url) {
  const client = redis.createClient({url, enable_offline_queue: false});
  client.on('error', function (err) {
    logger.error('Redis error', {
      error: err ? JSON.stringify(err) : null,
      stack: err ? err.stack : null,
      url,
    });
  });

  client.on('ready', () => {
    console.info('Redis client is ready ' + url);
  });

  return client;
}

module.exports = {
  createClient
};
