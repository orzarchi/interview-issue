const express = require("express");
const queries = require("./module-a-queries");

async function createApp() {
  const app = express();

  app.use('/sessions', (req, res) => {
    queries.setSessionInfo('session-id', {importantDetails: 'banana'})
      .then(() => {
        res.status(200).send('success');
      })
      .catch(()=>{
        res.sendStatus(500);
      });
  });

  return app;
}

module.exports = {
  createApp
};
