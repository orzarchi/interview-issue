const request = require('supertest')
const {createApp} = require("../express-app");

describe("get /sessions", () => {
  jest.setTimeout(10_0000);
  it("when passed a username and password", async () => {
    const app = await createApp();
    const response = await request(app).get("/sessions");
    expect(response.statusCode).toBe(200)
  })
})
